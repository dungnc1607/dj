//
//  ViewController.swift
//  DJSimulator
//
//  Created by Squall on 14/6/24.
//

import UIKit
import RxSwift
import RxCocoa
import Stevia
import AudioKit
import AudioKitEX
import AudioKitUI
import AVFAudio

class ViewController: UIViewController {
    // MARK: - Properties
    let disposeBag = DisposeBag()
    let engine = AudioEngine()
    let player = AudioPlayer()
    var buffer = AVAudioPCMBuffer()
    
    let btn: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Play", for: .normal)
        return btn
    }()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupEngine()
        binding()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        engine.stop()
    }
}

// MARK: - Private Function
extension ViewController {
    private func setupUI() {
        view.backgroundColor = .white
        view.subviews(btn)
        btn.centerInContainer()
    }
    
    private func binding() {
        btn.rx.tap.bind { [weak self] in
            guard let self = self else { return }
            if self.player.isPlaying {
                DispatchQueue.global(qos:.background).async {
                    self.player.stop()
                    DispatchQueue.main.async {
                        self.btn.setTitle("Play", for: .normal)
                    }
                }
            } else {
                self.loadFile(fileName: "cheeb-bd")
                self.player.start()
                self.btn.setTitle("Stop", for: .normal)
            }
        }.disposed(by: disposeBag)
    }
    
    private func setupEngine() {
        engine.output = player
        player.isLooping = true
        
        do {
            try engine.start()
        } catch {
            Log("Error starting AudioEngine:", error.localizedDescription)
        }
    }
    
    private func loadFile(fileName: String) {
        guard let url = Bundle.main.url(forResource: fileName, withExtension: "wav") else {
            Log("failed to load sample", fileName)
            return
        }
        
        do {
            let audioFile = try AVAudioFile(forReading: url)
            let fileFormat = audioFile.processingFormat
            let channelCount = fileFormat.channelCount
            
            let buffer = AVAudioPCMBuffer(pcmFormat: fileFormat, frameCapacity: AVAudioFrameCount(audioFile.length))
            try audioFile.read(into: buffer!)
            
            player.file = audioFile
            player.buffer = buffer
            player.isLooping = true
        } catch {
            Log("Error loading file:", error.localizedDescription)
        }
        
        player.file = try? AVAudioFile(forReading: url)
        player.isLooping = true
        player.buffer = buffer
    }
}
