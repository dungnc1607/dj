//
//  Helper.swift
//  DJSimulator
//
//  Created by Squall on 16/6/24.
//

import AudioKit
import AudioKitEX
import AudioKitUI
import AVFoundation

class Helper {
    static var sourceBuffer: AVAudioPCMBuffer {
        let url = Bundle.main.resourceURL?.appendingPathComponent("Sound/cheeb-bd.wav")
        let file = try! AVAudioFile(forReading: url!)
        return try! AVAudioPCMBuffer(file: file)!
    }
}
